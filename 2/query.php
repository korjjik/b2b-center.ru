<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 15.05.2018
 * Time: 16:09
 */

if ( ! function_exists('build_url'))
{
    /**
     * @param array $parts
     * @return string
     */
    function build_url(array $parts)
    {
        $scheme   = isset($parts['scheme']) ? ($parts['scheme'] . '://') : '';
        $host     = $parts['host'] ? $parts['host']: '';
        $port     = isset($parts['port']) ? (':' . $parts['port']) : '';
        $user     = isset($parts['user']) ? $parts['user']: '';
        $pass     = isset($parts['pass']) ? (':' . $parts['pass'])  : '';
        $pass     = ($user || $pass) ? ($pass . '@') : '';
        $path     = $parts['path'] ? $parts['path']: '';
        $query    = isset($parts['query']) ? ('?' . $parts['query']) : '';
        $fragment = isset($parts['fragment']) ? ('#' . $parts['fragment']) : '';
        return implode('', [$scheme, $user, $pass, $host, $port, $path, $query, $fragment]);
    }
}

function removeThree($var){
    return $var !== "3";
}

function cmp($a, $b) {
    //return $a <=> $b; php 7
    if ($a == $b) {
        return 0;
    }
    return $a > $b;
}

/**
 * @param string $url
 * @return string
 */
function magic($url){
    $parse_url = parse_url($url);
    $query = $parse_url["query"];
    parse_str($query, $query);
    $query = array_filter($query, "removeThree");
    uasort($query, 'cmp');
    $query["url"]=$parse_url["path"];
    $query= http_build_query($query);
    $parse_url["query"] = $query;
    $parse_url["path"] = "/";
    return build_url($parse_url);
}

$url = "https://www.somehost.com/test/index.html?param1=4&param2=3&param3=2&param4=1&param5=3";

var_dump(magic($url));
