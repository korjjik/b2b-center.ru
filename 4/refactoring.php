<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 15.05.2018
 * Time: 17:03
 */
// проблема в том что данные могут придти не валидные " ' или текст или может не существовать пользователя в базе
// напрмер
// 1,'," select ...
// 1,1,1,1
// 1,qwerty
// 1,9999,5    9999 - нету в базе
function load_users_data($user_ids) {
    $user_ids = explode(',', $user_ids);

    $host = 'localhost';
    $db   = 'database';
    $user = 'root';
    $charset = 'utf8';
    $password = "123123";
    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $pdo = new PDO($dsn, $user, $password);

    $in  = str_repeat('?,', count($user_ids) - 1) . '?';
    $stmt1 = $pdo->prepare("SELECT id, name FROM users WHERE id IN ($in)");

    $stmt1->execute($user_ids);
    $data = $stmt1->fetchAll(PDO::FETCH_KEY_PAIR);

    $stmt1 = null;
    $pdo = null;

    return $data;
}

// Как правило, в $_GET['user_ids'] должна приходить строка с номерами пользователей через запятую, например: 1,2,17,48
$data = load_users_data($_GET['user_ids']);
foreach ($data as $user_id=>$name) {
    echo "<a href=\"/show_user.php?id=$user_id\">$name</a>";
}
