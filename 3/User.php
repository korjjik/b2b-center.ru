<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\User
 *
 * @property int $user_id
 * @property string $first
 * @property string $last
 * @mixin \Eloquent
 */

class User extends Model
{
    protected $table = 'user';
    protected $primaryKey = 'user_id';
    protected $fillable = ['user_id', 'first', 'last'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function article()
    {
        return $this->hasMany('App\Article', 'user_id', 'user_id');
    }

    /**
     * @param array $data
     * @return $this - model
     */
    public function createArticele(array $data){
        $validator = \Validator::make($data, [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('post/create')
                ->withErrors($validator)
                ->withInput();
        }
        $article = new Article([
            "user_id" => $this->user_id,
            "name" => $data["name"]
        ]);
        $article->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getAllArticle(){
        return $this->article();
    }


}
