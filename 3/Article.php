<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Article
 *
 * @property int $article_id
 * @property int $user_id
 * @property string $name
 * @mixin \Eloquent
 */

class Article extends Model
{
    protected $table = 'article';
    protected $primaryKey = 'article_id';
    protected $fillable = ['article_id', 'user_id', 'name'];
    public $incrementing = true;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\User', 'user_id', "user_id");
    }

    /**
     * @return string - name Author
     */
    public function getAuthor(){
        return $this->user->first;
    }

    /**
     * @param array $user_id
     * @return $this|bool
     */
    public function changeAuthor(array $user_id){
        $validator = \Validator::make($user_id, [
            'user_id' => 'required|user:user_id',
        ]);

        if ($validator->fails()) {
            return redirect('post/create')
                ->withErrors($validator)
                ->withInput();
        }

        $this->user_id = $user_id["user_id"];
        return $this->save();
    }


}
