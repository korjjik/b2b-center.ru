<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 15.05.2018
 * Time: 13:23
 */

$host = '127.0.0.1';
$db   = 'test';
$user = 'root';
$charset = 'utf8';
$password = "";
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";

$pdo = new PDO($dsn, $user, $password);

$stmt1 = $pdo->prepare('INSERT INTO users (name, gender, birth_date) VALUES (?,?,?) ');
$stmt2 = $pdo->prepare('INSERT INTO phone_numbers (user_id, phone) VALUES (?,?) ');


for ($i = 0; $i < 100000; $i++){
    $int= mt_rand(863693507,1526381507);
    $stmt1->execute([str_shuffle("qwerty"), mt_rand(0,2), $int]);
    $stmt2->execute([mt_rand(0,100000), mt_rand(0,1000000000000)]);
}
$stmt1 = null;
$stmt2 = null;
$pdo = null;