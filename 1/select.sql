SELECT users.name, COUNT(phone_numbers.user_id) count_phone
FROM users
JOIN phone_numbers ON phone_numbers.user_id = users.user_id
WHERE users.gender = 1 AND users.birth_date > UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 22 YEAR)) AND users.birth_date < UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 18 YEAR))
GROUP BY phone_numbers.user_id ;